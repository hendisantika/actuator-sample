package com.hendisantika.actuatorsample;

import com.hendisantika.actuatorsample.entity.Account;
import com.hendisantika.actuatorsample.entity.Usernames;
import com.hendisantika.actuatorsample.repository.AccountRepository;
import com.hendisantika.actuatorsample.repository.UsernamesRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class ActuatorSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActuatorSampleApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository,
                           UsernamesRepository usernamesRepository) {
        return (evt) -> Arrays.asList(
                ("hendisantika,naruto,sasuke,sakura,kakashi,kiba,hinata,neji,shikamaru,choji,ino" +
                        ",shino,neji,tenten,lee").split(","))
                .forEach(
                        a -> {
                            Account account = accountRepository.save(new Account(a,
                                    "password"));
                            usernamesRepository.save(new Usernames(account,
                                    "http://example.com/login", a));
                        });
    }
}
