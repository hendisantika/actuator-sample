package com.hendisantika.actuatorsample.repository;

import com.hendisantika.actuatorsample.entity.Usernames;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.15
 * To change this template use File | Settings | File Templates.
 */
public interface UsernamesRepository extends JpaRepository<Usernames, Long> {

    Collection<Usernames> findByAccountUsername(String username);

}