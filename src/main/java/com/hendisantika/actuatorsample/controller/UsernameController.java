package com.hendisantika.actuatorsample.controller;

import com.hendisantika.actuatorsample.entity.Usernames;
import com.hendisantika.actuatorsample.exception.UserNotFoundException;
import com.hendisantika.actuatorsample.repository.AccountRepository;
import com.hendisantika.actuatorsample.repository.UsernamesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.19
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/{userId}/usernames")
public class UsernameController {

    private final UsernamesRepository usernamesRepository;
    private final AccountRepository accountRepository;

    @Autowired
    UsernameController(UsernamesRepository usernamesRepository, AccountRepository accountRepository) {
        this.usernamesRepository = usernamesRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping
    Collection<Usernames> readUsernames(@PathVariable String userId) {
        this.validateUser(userId);
        return this.usernamesRepository.findByAccountUsername(userId);
    }

    @PostMapping
    ResponseEntity<?> add(@PathVariable String userId, @RequestBody Usernames input) {
        this.validateUser(userId);

        return this.accountRepository.findByUsername(userId)
                .map(account -> {
                    Usernames result = usernamesRepository.save(new Usernames(account, input.url, input.username));

                    URI url = ServletUriComponentsBuilder
                            .fromCurrentRequest().path("/{id}")
                            .buildAndExpand(result.getId()).toUri();

                    return ResponseEntity.created(url).build();
                })
                .orElse(ResponseEntity.noContent().build());
    }

    @GetMapping(value = "{usernamesId}")
    Optional<Usernames> readUsername(@PathVariable String userId, @PathVariable Long usernameId) {
        this.validateUser(userId);
        return this.usernamesRepository.findById(usernameId);
    }

    private void validateUser(String userId) {
        this.accountRepository.findByUsername(userId).orElseThrow(
                () -> new UserNotFoundException(userId));
    }


}