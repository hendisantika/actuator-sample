package com.hendisantika.actuatorsample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.21
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping
public class HomeController {
    @GetMapping
    String home() {
        return "Spring Boot Actuator Sample! " + new Date();
    }
}
