package com.hendisantika.actuatorsample.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.09
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    @JsonIgnore
    public String password;
    public String username;

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "account")
    private Set<Usernames> usernames = new HashSet<>();

    public Account(String name, String password) {
        this.username = name;
        this.password = password;
    }
}
