package com.hendisantika.actuatorsample.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.11
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Data
public class Usernames {
    public String url;
    public String username;

    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Account account;

    Usernames() {

    }

    public Usernames(Account account, String url, String username) {
        this.url = url;
        this.account = account;
        this.username = username;
    }

}
