package com.hendisantika.actuatorsample.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : actuator-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/18
 * Time: 08.18
 * To change this template use File | Settings | File Templates.
 */
public class UserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 7537022054146700535L;

    public UserNotFoundException(String userId) {
        super("Sorry, we could not find user '" + userId + "'.");
    }


}