# Spring Boot Actuator Example

[![pipeline status](https://gitlab.com/hendisantika/actuator-sample/badges/master/pipeline.svg)](https://gitlab.com/hendisantika/actuator-sample/commits/master)
[![coverage report](https://gitlab.com/hendisantika/actuator-sample/badges/master/coverage.svg)](https://gitlab.com/hendisantika/actuator-sample/commits/master)

#### To ru this repo :

`mvn clean spring-boot:run`

#### REST queries for data in repositories

Query for usernames belonging to account naruto.

`curl localhost:8080/naruto/usernames`

Query for usernames belonging to account sasuke.

`curl localhost:8080/sasuke/usernames`

#### Actuator queries

The response to this query is truncated because it is really, really long.

#### Beans

`curl localhost:8080/beans`

#### Metrics

`curl localhost:8080/metrics`





